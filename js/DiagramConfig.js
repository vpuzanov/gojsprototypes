define(
    ['lib/go-debug', './NodeTemplateMap', 'common/LinkTemplate', 'common/GroupTemplate', 'layouts/CustomLayout'],
    function (go, NodeTemplateMap, LinkTemplate, GroupTemplate, CustomLayout) {
        var $ = go.GraphObject.make;

        return {
            layout: $(CustomLayout),
            nodeTemplateMap: NodeTemplateMap,
            linkTemplate: LinkTemplate,
            groupTemplate: GroupTemplate,
            allowDrop: true,
            mouseDrop: function(e) {
                var diagram = e.diagram;
                if (! diagram.commandHandler.addTopLevelParts(diagram.selection, true)) {
                    diagram.currentTool.doCancel();
                }
            },
            'draggingTool.dragsLink': false,
            'linkingTool.isUnconnectedLinkValid': false,
            'relinkingTool.isUnconnectedLinkValid': false,
            'relinkingTool.fromHandleArchetype': $(go.Shape, 'Diamond', {
                segmentIndex: 0,
                cursor: 'pointer',
                desiredSize: new go.Size(8, 8),
                fill: '#ED4255',
                stroke: null
            }),
            'relinkingTool.toHandleArchetype': $(go.Shape, 'Diamond', {
                segmentIndex: -1,
                cursor: 'pointer',
                desiredSize: new go.Size(8, 8),
                fill: '#ED4255',
                stroke: null
            }),
            'linkReshapingTool.handleArchetype': $(go.Shape, 'Diamond', {
                desiredSize: new go.Size(7, 7),
                fill: 'lightblue',
                stroke: null
            }),
            'undoManager.isEnabled': true
        };
    }
);