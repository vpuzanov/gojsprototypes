define(
    ['./NodeTemplateMap', 'common/GroupTemplate'],
    function (NodeTemplateMap, GroupTemplate) {
        return {
            maxSelectionCount: 1,
            nodeTemplateMap: NodeTemplateMap,
            groupTemplate: GroupTemplate
        };
    }
);