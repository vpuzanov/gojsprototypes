define(
    ['lib/go-debug', 'figures/CircleTemplate', 'figures/TriangleTemplate', 'figures/SquareTemplate'],
    function (go, CircleTemplate, TriangleTemplate, SquareTemplate) {
        var nodeTemplateMap = new go.Map('string', go.Node);
        nodeTemplateMap.add('circles', CircleTemplate);
        nodeTemplateMap.add('triangles', TriangleTemplate);
        nodeTemplateMap.add('squares', SquareTemplate);

        return nodeTemplateMap;
    }
);