define(['lib/go-debug'], function (go) {
    var CustomLayout = function () {
        go.Layout.call(this);

        this._spacing = new go.Size(150, 150);
    };

    go.Diagram.inherit(CustomLayout, go.Layout);

    Object.defineProperty(CustomLayout.prototype, 'spacing', {
        get: function() { return this._spacing; },
        set: function(val) {
            if (!this._spacing.equals(val)) {
                this._spacing = val;
                this.invalidateLayout();
            }
        }
    });

    CustomLayout.prototype.doLayout = function (nodes) {
        if (nodes === this.diagram) {
            nodes = this.diagram.nodes;
        } else if (nodes === this.group) {
            nodes = this.group.memberParts;
        }

        var root = null;
        var iterator = nodes.iterator;
        while (iterator.next()) {
            var n = iterator.value;
            if (!(n instanceof go.Node)) {
                continue;
            }
            if (root === null) {
                root = n;
            }
            if (n.findLinksInto().count === 0) {
                root = n;
                break;
            }
        }

        if (root === null) {
            return;
        }

        this.diagram.startTransaction('Custom Layout');

        positionNodes(root, this.arrangementOrigin.x, this.arrangementOrigin.y, this.spacing);

        this.diagram.commitTransaction('Custom Layout');
    };

    function positionNodes(startNode, x, y, spacing) {
        startNode.move(new go.Point(x, y));

        var linksFrom = startNode.findLinksOutOf();

        linksFrom.each(function (link) {
            var x, y;

            var nextNode = link.toNode;

            if (nextNode === null) {
                return false;
            }

            var nodeBounds = nextNode.actualBounds;

            if ((startNode.category === 'squares' && (nextNode.category === 'circles' || nextNode.category === 'squares')) ||
                (startNode.category === 'circles' && (nextNode.category === 'squares' || nextNode.category === 'circles'))) {

                x = startNode.position.x + nodeBounds.width + spacing.width;
                y = startNode.position.y;

                if (link !== null) {
                    link.fromSpot = go.Spot.Right;
                    link.toSpot = go.Spot.Left;
                }
            } else if ((startNode.category === 'squares' && (nextNode.category === 'triangles' || nextNode.category === 'squares')) ||
                (startNode.category === 'triangles' && (nextNode.category === 'squares' || nextNode.category === 'triangles'))) {

                x = startNode.position.x;
                y = startNode.position.y + nodeBounds.height + spacing.height;

                if (link !== null) {
                    link.fromSpot = go.Spot.Bottom;
                    link.toSpot = go.Spot.Top;
                }
            } else if (startNode.category === 'group' && nextNode.category === 'group') {
                x = startNode.position.x + nodeBounds.width + spacing.width;
                y = startNode.position.y;

                if (link !== null) {
                    link.fromSpot = go.Spot.Right;
                    link.toSpot = go.Spot.Left;
                }
            }

            positionNodes(nextNode, x, y, spacing);
        });
    }

    return CustomLayout;
});