define(['lib/go-debug'], function(go) {
    var $ = go.GraphObject.make;

    var nodeDefaults = {
        name: 'node',
        locationSpot: go.Spot.Center,
        selectable: true,
        selectionAdornmentTemplate: $(go.Adornment, 'Auto',
            $(go.Shape, {fill: null, stroke: 'lightblue', strokeWidth: 1.5, strokeDashArray: [4, 2]}),
            $(go.Placeholder, {padding: 5})
        ),
        mouseDrop: function(e, node) {
            var group = node.containingGroup;

            if (!(group !== null && group.addMembers(group.diagram.selection, true))) {
                group.diagram.currentTool.doCancel();
            } else {
                group.layout.doLayout(group);
            }
        },
        linkConnected: function(node) {
            node.diagram.nodes.each(function(node) {
                node.isHighlighted = false;
            });
        },
        linkValidation: function(fromNode, fromPort, toNode) {
            var allow = !((fromNode.category === 'triangles' && toNode.category === 'circles') ||
            (fromNode.category === 'circles' && toNode.category === 'triangles'));

            if (! allow) {
                toNode.isHighlighted = true;
            }

            return allow;
        }
    };

    var outerShapeDefaults = {
        portId: '',
        fromLinkable: true,
        toLinkable: true,
        cursor: 'pointer'
    };

    var innerShapeDefaults = {
        fill: '#fff',
        stroke: null,
        margin: 5,
        width: 60,
        height: 60
    };

    var textBlockDefaults = {
        font: 'bold 11px Helvetica, Arial, sans-serif',
        margin: 8,
        maxSize: new go.Size(160, NaN),
        wrap: go.TextBlock.WrapFit
    };

    return {
        nodeDefaults: nodeDefaults,
        outerShapeDefaults: outerShapeDefaults,
        innerShapeDefaults: innerShapeDefaults,
        textBlockDefaults: textBlockDefaults
    }
});