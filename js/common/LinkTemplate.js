define(['lib/go-debug'], function (go) {
    var $ = go.GraphObject.make;

    var linkEndingMap = {
        Square: 'Block'
    };

    return $(go.Link,
        {
            name: 'figureLink',
            selectable: true,
            selectionAdornmentTemplate: $(go.Adornment, 'Link', $(go.Shape, {
                isPanelMain: true,
                fill: 'deepskyblue',
                stroke: 'deepskyblue',
                strokeWidth: 1.5
            })),
            relinkableFrom: true,
            relinkableTo: true,
            reshapable: true
        },
        $(go.Shape, {isPanelMain: true}),
        $(go.Shape, {stroke: null},
            new go.Binding('fromArrow', 'fromNode', function(node) {
                if (node.category === 'group') {
                    return '';
                }
                var targetInnerFigure = node.findObject('innerFigure').figure;
                return linkEndingMap[targetInnerFigure] || targetInnerFigure;
            }).ofObject('figureLink')
        ),
        $(go.Shape, {stroke: null},
            new go.Binding('toArrow', 'toNode', function(node) {
                if (node.category === 'group') {
                    return 'Standard';
                }
                var targetInnerFigure = node.findObject('innerFigure').figure;
                return linkEndingMap[targetInnerFigure] || targetInnerFigure;
            }).ofObject('figureLink')
        ),
        $(go.Panel, 'Auto',
            $(go.Shape, 'RoundedRectangle', {fill: '#fff', stroke: '#C0C0C0'}),
            $(go.TextBlock, {
                    name: 'linkText',
                    textAlign: 'center',
                    font: '10px helvetica, arial, sans-serif',
                    margin: 2,
                    minSize: new go.Size(10, NaN)
                },
                new go.Binding('text', '', function(link) {
                    return link.fromNode.findObject('textBlock').text + ' to ' + link.toNode.findObject('textBlock').text;
                }).ofObject('figureLink')
            )
        )
    );
});