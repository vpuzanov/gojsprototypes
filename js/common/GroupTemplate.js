define(['lib/go-debug', 'layouts/CustomLayout'], function (go, CustomLayout) {
    var $ = go.GraphObject.make;

    return $(go.Group, 'Vertical', {
            ungroupable: true,
            selectable: true,
            selectionAdornmentTemplate: $(go.Adornment, 'Auto',
                $(go.Shape, {fill: null, stroke: 'lightblue', strokeWidth: 1.5, strokeDashArray: [4, 2]}),
                $(go.Placeholder, {padding: 5})
            ),
            mouseDrop: function(e, group) {
                if (!(group !== null && group.addMembers(group.diagram.selection, true))) {
                    group.diagram.currentTool.doCancel();
                } else {
                    group.layout.doLayout(group);
                }
            },
            computesBoundsAfterDrag: true,
            memberValidation: function (group, part) {
                return true;
            },
            linkValidation: function(fromNode, fromPort, toNode) {
                return fromNode.category === 'group' && toNode.category === 'group';
            },
            layout: $(CustomLayout)
        },
        $(go.Panel, 'Auto',
            $(go.Shape, 'RoundedRectangle', {
                fill: '#E5E5E5',
                stroke: '#8F8F91',
                portId: '',
                fromLinkable: true,
                toLinkable: true,
                cursor: 'pointer'
            }),
            $(go.Placeholder, {padding: 10}),
            $(go.Shape, 'RoundedRectangle', {fill: '#E5E5E5', stroke: null})
        ),
        $(go.TextBlock, 'Group', {
            name: 'textBlock',
            alignment: go.Spot.Bottom,
            font: 'Bold 12px helvetica, arial, sans-serif',
            margin: 8
        })
    );
});