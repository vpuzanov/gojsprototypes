define(['lib/go-debug', 'utils/Utils', 'common/NodeTemplateDefaults'], function (go, Utils, NodeTemplateDefaults) {
    var $ = go.GraphObject.make;

    return $(go.Node, 'Auto',
        NodeTemplateDefaults.nodeDefaults,
        $(go.Panel, 'Auto',
            $(go.Shape, 'Circle', {name: 'outerFigure'}, Utils.extend({}, NodeTemplateDefaults.outerShapeDefaults, {
                    fill: $(go.Brush, go.Brush.Pattern, { pattern: document.getElementById('circle-pattern')})
                }),
                new go.Binding('stroke', 'isHighlighted', function(h) { return h ? '#F24E4E' : '#808080' }).ofObject('node')
            ),
            $(go.Shape, 'Square', {name: 'innerFigure'}, Utils.extend({}, NodeTemplateDefaults.innerShapeDefaults, {stroke: '#808080'})),
            $(go.TextBlock, 'Circle', {name: 'textBlock'}, Utils.extend({}, NodeTemplateDefaults.textBlockDefaults, {stroke: '#808080'}))
        )
    );
});