define(['lib/go-debug', 'utils/Utils', 'common/NodeTemplateDefaults'], function (go, Utils, NodeTemplateDefaults) {
    var $ = go.GraphObject.make;

    return $(go.Node, 'Auto',
        NodeTemplateDefaults.nodeDefaults,
        $(go.Panel, 'Auto',
            $(go.Shape, 'Square', {name: 'outerFigure'}, Utils.extend({}, NodeTemplateDefaults.outerShapeDefaults, {
                    fill: $(go.Brush, go.Brush.Pattern, { pattern: document.getElementById('square-pattern')})
                }),
                new go.Binding('stroke', 'isHighlighted', function(h) { return h ? '#F24E4E' : '#4F76C8' }).ofObject('node')
            ),
            $(go.Shape, 'Triangle', {name: 'innerFigure'}, Utils.extend({}, NodeTemplateDefaults.innerShapeDefaults, {width: 100, height: 100, stroke: '#4F76C8'})),
            $(go.TextBlock, 'Square', {name: 'textBlock'}, Utils.extend({}, NodeTemplateDefaults.textBlockDefaults, {stroke: '#4F76C8'}))
        )
    );
});