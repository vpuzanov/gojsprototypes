define(['lib/go-debug', 'utils/Utils', 'common/NodeTemplateDefaults'], function (go, Utils, NodeTemplateDefaults) {
    var $ = go.GraphObject.make;

    return $(go.Node, 'Auto',
        NodeTemplateDefaults.nodeDefaults,
        $(go.Panel, 'Auto',
            $(go.Shape, 'Triangle', {name: 'outerFigure'}, Utils.extend({}, NodeTemplateDefaults.outerShapeDefaults, {
                    fill: $(go.Brush, go.Brush.Pattern, { pattern: document.getElementById('triangle-pattern')})
                }),
                new go.Binding('stroke', 'isHighlighted', function(h) { return h ? '#F24E4E' : '#009688' }).ofObject('node')
            ),
            $(go.Shape, 'Circle', {name: 'innerFigure'},  Utils.extend({}, NodeTemplateDefaults.innerShapeDefaults, {stroke: '#009688'})),
            $(go.TextBlock, 'Triangle', {name: 'textBlock'}, Utils.extend({}, NodeTemplateDefaults.textBlockDefaults, {stroke: '#009688'}))
        )
    );
});