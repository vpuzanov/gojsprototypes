require.config({
    shim: {
        'lib/go-debug': {
            exports: 'go'
        }
    }
});

require(['lib/go-debug', './PaletteConfig', './DiagramConfig'], function(go, PaletteConfig, DiagramConfig) {
    var $ = go.GraphObject.make;

    var palette = $(go.Palette, 'palette', PaletteConfig);
    var diagram = $(go.Diagram, 'diagram', DiagramConfig);

    palette.model = new go.GraphLinksModel([
        {category: 'circles'},
        {category: 'triangles'},
        {category: 'squares'},
        {category: 'group', isGroup: true}
    ]);

    diagram.model.nodeDataArray = [];
});